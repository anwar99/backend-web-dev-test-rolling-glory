exports.getAllMapper = (status,message,data)=>{    
    let datas = data.data.length==0 ? null : []
    var check = data.data.length==0 ? null : data.data.map(val=>{
        var obj = {
            id:val.id,
            product_id: val.product_id,
            reedem_points : val.p_reedem_point,
            include:{
                product:{
                    product_name : val.p_name,
                    product_description : val.p_description,
                    product_rincian : val.p_rincian,
                    product_image : val.p_image, 
                    stock: val.sp_stock                   
                },
            }
        }
        datas.push(obj)
    })

    return {
        status:status,
        message:message,
        data:datas,
        links:{
            prev:data.pagination.currentPage <= 1 ? process.env.BASE_URL+`gifts?per_page=${data.pagination.perPage}&current_page=${data.pagination.currentPage}&include[product]=true` : process.env.BASE_URL+`gifts?per_page=${data.pagination.perPage}&current_page=${parseInt(data.pagination.currentPage)-1}&include[product]=true` ,
            current:process.env.BASE_URL+`gifts?per_page=${data.pagination.perPage}&current_page=${data.pagination.currentPage}&include[product]=true`,
            next:process.env.BASE_URL+`gifts?per_page=${data.pagination.perPage}&current_page=${parseInt(data.pagination.currentPage)+1}&include[product]=true`
        }
    }
}

exports.getByIdMapper = (status,message,data)=>{
    var obj = !data ? null : {
        id:data.id,
        product_id: data.product_id,    
        reedem_points : data.p_reedem_point,
        include:{
            product:{
                product_name : data.p_name,
                product_description : data.p_description,
                product_rincian : data.p_rincian,
                product_image : data.p_image,
                stock: data.sp_stock
            }
        }
    }
    return {
        status:status,
        message:message,
        data:obj,        
    }
}