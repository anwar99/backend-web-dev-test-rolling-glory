exports.userMapper = (status,message,data)=>{
    return {
        status:status,
        message:message,
        data:{
            fullname : data.firstname+" "+data.lastname,
            firstname : data.firstname,
            lastname : data.lastname,
            email : data.email,
            phone : data.phone,
            token : data.token
        }
    }
}