exports.reviewsMapper = (status,message,data)=>{
    return {
        status:status,
        message:message,
        data:{
            comments:data.comments,
            ratings:data.ratings,
            product:{
                product_name : data.p_name,
                product_description : data.p_description,
                product_rincian : data.p_rincian,
                product_image : data.p_image
            },
            user:{
                fullname : data.firstname+" "+data.lastname,
                firstname : data.firstname,
                lastname : data.lastname,
                email : data.email,
                phone : data.phone,
                token : data.token
            }
        }
    }
}