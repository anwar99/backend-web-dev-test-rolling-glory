
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('kategori').del()
    .then(async function () {
      var data = [
        {
          k_name : "Best Seller",
        },
        {
          k_name : "Hot Item",
        },
        {
          k_name : "New",
        }
      ]
      // Inserts seed entries
      return await knex('kategori').insert(data);
    });
};
