require('dotenv').config()
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(async function () {
      let data = [
          {
              "p_name": "Samsung Galaxy S9 -Midnight Black 4/64 GB",
              "p_description":"Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo)",
              "p_rincian" : "Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo) CPU: Exynos 9810 Octa-core (2.7GHz Quad + 1.7GHz Quad), 64 bit, 10nm processor Kamera: Super Speed Dual Pixel, 12 MP OIS (F1.5/F2.4 Dual Aperture) + 12MP OIS (F2.4) with LED flash, depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless",
              "p_image" : "http://localhost:5000/images/samsung.png",
          },
          {
              "p_name": "Samsung Galaxy A52 - White 8GB/256GB",
              "p_description":"Ukuran layar:  6.5 inci,  Super AMOLED, 90Hz, 800 nits (peak) 1080 x 2400 pixels, 20:9 ratio (~405 ppi density), 18.5:9 Memori: RAM 8 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11",
              "p_rincian" : "Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11 CPU: Qualcomm SM7125 Snapdragon 720G 8nm (Octa-core 2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver), 64 bit, 10nm processor Kamera:  Belakang 64 MP, f/1.8, 26mm (wide), 1/1.7X, 0.8µm, PDAF, OIS 12 MP, f/2.2, 123˚ (ultrawide), 1.12µm 5 MP, f/2.4, (macro) 5 MP, f/2.4, (depth), depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless",
              "p_image" : "http://localhost:5000/images/samsung a52.jpeg",              
          },
          {
            "p_name": "Samsung Galaxy S8 -Midnight Black 4/64 GB",
            "p_description":"Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo)",
            "p_rincian" : "Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo) CPU: Exynos 9810 Octa-core (2.7GHz Quad + 1.7GHz Quad), 64 bit, 10nm processor Kamera: Super Speed Dual Pixel, 12 MP OIS (F1.5/F2.4 Dual Aperture) + 12MP OIS (F2.4) with LED flash, depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless",
            "p_image" : "http://localhost:5000/images/samsung s8.png",
        },
        {
            "p_name": "Samsung Galaxy A52s 5G - White 8GB/256GB",
            "p_description":"Ukuran layar:  6.5 inci,  Super AMOLED, 90Hz, 800 nits (peak) 1080 x 2400 pixels, 20:9 ratio (~405 ppi density), 18.5:9 Memori: RAM 8 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11",
            "p_rincian" : "Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11 CPU: Qualcomm SM7125 Snapdragon 720G 8nm (Octa-core 2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver), 64 bit, 10nm processor Kamera:  Belakang 64 MP, f/1.8, 26mm (wide), 1/1.7X, 0.8µm, PDAF, OIS 12 MP, f/2.2, 123˚ (ultrawide), 1.12µm 5 MP, f/2.4, (macro) 5 MP, f/2.4, (depth), depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless",
            "p_image" : "http://localhost:5000/images/samsung a52s.jpeg",              
        },
        {
            "p_name": "Samsung Galaxy A72 - White 8GB/256GB",
            "p_description":"Ukuran layar:  6.5 inci,  Super AMOLED, 90Hz, 800 nits (peak) 1080 x 2400 pixels, 20:9 ratio (~405 ppi density), 18.5:9 Memori: RAM 8 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11",
            "p_rincian" : "Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 256 GB, Sistem operasi: Android 11 CPU: Qualcomm SM7125 Snapdragon 720G 8nm (Octa-core 2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver), 64 bit, 10nm processor Kamera:  Belakang 64 MP, f/1.8, 26mm (wide), 1/1.7X, 0.8µm, PDAF, OIS 12 MP, f/2.2, 123˚ (ultrawide), 1.12µm 5 MP, f/2.4, (macro) 5 MP, f/2.4, (depth), depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless",
            "p_image" : "http://localhost:5000/images/samsung a72.jpeg",              
        }
      ]
      // Inserts seed entries
      return await knex('products').insert(data);
    });
};
