
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('products_kategori').del()
    .then(async function () {
      var product = await knex.select('id').from('products');
      var kategori = await knex.select('id').from('kategori')
      var data = [
        {
          product_id:product[0].id,
          kategori_id:kategori[0].id
        },
        {
          product_id:product[1].id,
          kategori_id:kategori[2].id
        },
        {
          product_id:product[3].id,
          kategori_id:kategori[1].id
        },
        {
          product_id:product[2].id,
          kategori_id:kategori[2].id
        },
        {
          product_id:product[4].id,
          kategori_id:kategori[2].id
        },
      ]
      // Inserts seed entries
      return await knex('products_kategori').insert(data);
    });
};
