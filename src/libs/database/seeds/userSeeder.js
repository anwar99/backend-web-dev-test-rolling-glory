const bcrypt = require('bcrypt')

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then( async function () {
      // Inserts seed entries
      const data = [
        {
          "firstname":"John",
          "lastname":"Doe",
          "username":"jhondoe",
          "email":"jhondoe@example.com",
          "phone":"081234567891",
          "password":bcrypt.hashSync("12345678",10)          
        },
        {
          "firstname":"Jane",
          "lastname":"Doe",
          "username":"janedoe",
          "email":"janedoe@example.com",
          "phone":"081234567892",
          "password":bcrypt.hashSync("12345678",10)          
        },
      ]
      return await knex('users').insert(data);
    });
};
