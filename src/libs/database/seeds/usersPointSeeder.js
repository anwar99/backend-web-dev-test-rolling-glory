
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('user_points').del()
    .then(async function () {      
      // Inserts seed entries
      let user = await knex.select('id').from('users')
      let data = [];
      user.map(async val => {
        var obj = {
          user_id : val.id,
          up_point : 500000
        }
        data.push(obj)
      })      
      return await knex('user_points').insert(data);
    });
};
