exports.seed = async function(knex) {
  // Deletes ALL existing entries
  return knex('stock_product').del()
    .then(async function () {
      // Inserts seed entries
      var product = await knex.select('id').from('products');
      var kategori = await knex.select('id').from('kategori')
      var data = [
        {
          product_id:product[0].id,
          sp_stock:6,
        },
        {
            product_id:product[1].id,
            sp_stock:14,
        },
        {
            product_id:product[2].id,
            sp_stock:10,
          },
          {
              product_id:product[3].id,
              sp_stock:3,
          },
          {
            product_id:product[4].id,
            sp_stock:15,
          }
      ]
      return await knex('stock_product').insert(data);
    });
};
