exports.up = function(knex) {
    return knex.schema.createTable('users',(table)=>{
        table.increments("id")
        table.string('firstname',100).notNullable()
        table.string('lastname',100).notNullable()
        table.string('username',10).notNullable().unique()
        table.string('email',50).notNullable().unique()
        table.string('phone',13).notNullable().unique()
        table.string('password',191).notNullable()
        table.timestamps()
      })
};
  
exports.down = function(knex) {
return knex.schema.dropTable('users')
};