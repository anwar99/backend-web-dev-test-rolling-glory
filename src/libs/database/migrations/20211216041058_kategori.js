
exports.up = function(knex) {
    return knex.schema.createTable('kategori',(table)=>{
        table.increments("id")              
        table.string('k_name',25).notNullable()
        table.timestamps()
    })
};

exports.down = function(knex) {
    knex.schema.table('kategori',table=>{
        table.dropForeign('product_id',['fk_fkey_product_label'])       
    })
    return knex.schema
    .dropTable('kategori')
};
