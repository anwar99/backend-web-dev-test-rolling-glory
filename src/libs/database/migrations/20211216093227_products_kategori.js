
exports.up = function(knex) {
    return knex.schema.createTable('products_kategori',(table)=>{
        table.increments("id")
        table.integer('product_id').unsigned().notNullable();
        table.foreign('product_id').references('products.id').withKeyName('fk_fkey_products_kategori').onDelete('cascade').onUpdate('cascade');
        table.integer('kategori_id').unsigned().notNullable();
        table.foreign('kategori_id').references('kategori.id').withKeyName('fk_fkey_kategori').onDelete('cascade').onUpdate('cascade');        
    })
};

exports.down = function(knex) {
    knex.schema.table('products_kategori',table=>{
        table.dropForeign('product_id',['fk_fkey_products_kategori'])     
        table.dropForeign('kategori_id',['fk_fkey_kategori'])       
    })
    return knex.schema
    .dropTable('products_kategori')
};
