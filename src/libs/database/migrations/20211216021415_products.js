
exports.up = function(knex) {
  return knex.schema.createTable('products',(table)=>{
    table.increments("id")
    table.string('p_name',100).notNullable()
    table.string('p_description').notNullable()
    table.string('p_rincian').notNullable()    
    table.string('p_image',100).notNullable()        
    table.timestamps()
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('products')
};
