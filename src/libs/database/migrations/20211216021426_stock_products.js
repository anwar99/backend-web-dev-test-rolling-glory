
exports.up = function(knex) {
    return knex.schema.createTable('stock_product',(table)=>{
        table.increments("id")
        table.integer('product_id').unsigned().notNullable();
        table.foreign('product_id').references('products.id').withKeyName('fk_fkey_products').onDelete('cascade').onUpdate('cascade');
        table.bigInteger('sp_stock').notNullable()
        table.timestamps()
      })
};

exports.down = function(knex) {
    knex.schema.table('stock_product',table=>{
        table.dropForeign('product_id',['fk_fkey_products'])       
    })
    return knex.schema
    .dropTable('stock_product')
};
