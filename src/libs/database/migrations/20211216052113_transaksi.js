
exports.up = function(knex) {
    return knex.schema.createTable('reedem_transaksi',(table)=>{
        table.increments("id")
        table.integer('product_gift_id').unsigned().notNullable();
        table.foreign('product_gift_id').references('products_gifts.id').withKeyName('fk_fkey_transaksi_products_gifts').onDelete('cascade').onUpdate('cascade');
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references('users.id').withKeyName('fk_fkey_transaksi_user').onDelete('cascade').onUpdate('cascade');
        table.integer('jumlah_item').notNullable()
        table.integer('bayar_point')
        table.timestamps()
    })
};

exports.down = function(knex) {
    knex.schema.table('reedem_transaksi',table=>{ 
        table.dropForeign('product_id',['fk_fkey_transaksi_products_gifts'])  
        table.dropForeign('user_id',['fk_fkey_transaksi_user'])        
    })
    return knex.schema
    .dropTable('reedem_transaksi')
};
