
exports.up = function(knex) {
    return knex.schema.createTable('wishlist',(table)=>{
        table.increments("id")
        table.integer('product_id').unsigned().notNullable();
        table.foreign('product_id').references('products.id').withKeyName('fk_fkey_wishlist_product').onDelete('cascade').onUpdate('cascade');
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references('users.id').withKeyName('fk_fkey_wishlist_user').onDelete('cascade').onUpdate('cascade');        
        table.timestamps()
    })
};

exports.down = function(knex) {
    knex.schema.table('wishlist',table=>{
        table.dropForeign('product_id',['fk_fkey_wishlist_product']) 
        table.dropForeign('user_id',['fk_fkey_wishlist_user'])  
    })
    return knex.schema
    .dropTable('wishlist')
};
