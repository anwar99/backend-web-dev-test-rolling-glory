
exports.up = function(knex) {
    return knex.schema.createTable('products_gifts',(table)=>{
        table.increments("id")
        table.integer('product_id').unsigned().notNullable();
        table.foreign('product_id').references('products.id').withKeyName('fk_fkey_products_gifts').onDelete('cascade').onUpdate('cascade'); 
        table.bigInteger('p_reedem_point').notNullable()     
        table.timestamps()
    })
};

exports.down = function(knex) {
    knex.schema.table('products_gifts',table=>{
        table.dropForeign('product_id',['fk_fkey_products_gifts']) 
    })
    return knex.schema
    .dropTable('products_gifts')
};
