
exports.up = function(knex) {
    return knex.schema.createTable('user_points',(table)=>{
        table.increments("id")
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references('users.id').withKeyName('fk_fkey_user_points').onDelete('cascade').onUpdate('cascade');
        table.bigInteger('up_point')        
    })
};

exports.down = function(knex) {
    knex.schema.table('user_points',table=>{ 
        table.dropForeign('user_id',['fk_fkey_user_points'])       
    })
    return knex.schema
    .dropTable('user_points')
};
