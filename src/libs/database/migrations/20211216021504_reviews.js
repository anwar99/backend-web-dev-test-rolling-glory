
exports.up = function(knex) {
    return knex.schema.createTable('reviews',(table)=>{
        table.increments("id")
        table.integer('product_id').unsigned().notNullable();
        table.foreign('product_id').references('products.id').withKeyName('fk_fkey_products_reviews').onDelete('cascade').onUpdate('cascade');
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references('users.id').withKeyName('fk_fkey_reviews_user').onDelete('cascade').onUpdate('cascade');
        table.string('ratings').notNullable()
        table.text('comments')        
        table.timestamps()
    })
};

exports.down = function(knex) {
    knex.schema.table('reviews',table=>{
        table.dropForeign('product_id',['fk_fkey_products_reviews'])  
        table.dropForeign('user_id',['fk_fkey_reviews_user'])       
    })
    return knex.schema
    .dropTable('reviews')
};
