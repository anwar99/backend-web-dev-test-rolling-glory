
class User {
    constructor(request) {
      this.firstname = request.firstname;
      this.lastname = request.lastname;
      this.email = request.email;
      this.phone = request.phone;
      this.password = request.password;
    }
}
  
module.exports = User;