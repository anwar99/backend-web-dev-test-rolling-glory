class Gifts {
    constructor(request) {
      this.id = request.params.id || null,
      this.product_id = request.body.product_id || null,
      this.p_reedem_point = request.body.p_reedem_point || null,
      this.p_name = request.body.p_name || null,
      this.p_description = request.body.p_description || null,
      this.p_rincian = request.body.p_rincian || null,
      this.query = request.query || null
    }
}
  
module.exports = Gifts;