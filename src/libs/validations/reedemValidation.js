const Joi = require('joi')
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
    product_id : Joi.array().required(),
    user_id : Joi.array().required(),
    jumlah_item : Joi.array().min(1).required(),
})

module.exports = validator.body(schema)