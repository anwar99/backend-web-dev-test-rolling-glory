const Joi = require('joi')
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
    firstname : Joi.string().required(),
    lastname : Joi.string().required(),
    email : Joi.string().email().required(),
    phone : Joi.number().min(10).max(13).required(),
    password : Joi.string().min(8).required(),
})

module.exports = schema