const Joi = require('joi')
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
    email : Joi.string().email().required(),
    password : Joi.string().min(8).required(),
})

module.exports = validator.body(schema)