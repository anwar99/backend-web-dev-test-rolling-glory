const Joi = require('joi')
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
    product_id : Joi.number().required(),
    p_reedem_point : Joi.number().required(),
})

module.exports = validator.body(schema)