const Joi = require('joi')
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
    p_name : Joi.string().required(),
    p_description : Joi.string().required(),    
    p_rincian : Joi.string().required(),
})

module.exports = validator.body(schema)