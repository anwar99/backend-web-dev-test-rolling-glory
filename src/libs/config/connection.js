const knex = require('knex')
const knexfile = require("../database/knexfile")
const { attachPaginate } = require('knex-paginate');
attachPaginate();
const db = knex(knexfile.development)


module.exports = db