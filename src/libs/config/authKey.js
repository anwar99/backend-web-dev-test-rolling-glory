const fs = require('fs')
const path = require('path');
const privateKey = fs.readFileSync(path.resolve("src/libs/privatekey.pem"),'utf8')
const publicKey = fs.readFileSync(path.resolve("src/libs/publickey.pem"),'utf8')
var key = {
    'private':privateKey,
    'public':publicKey
}

module.exports = key