const db = require("../config/connection")

exports.giftById = async (data)=>{
    var where = data.id == undefined ? {} : {"products_gifts.id":data.id}
    return await db('products_gifts')
    .select('products_gifts.id',
        'products_gifts.product_id',
        'products_gifts.p_reedem_point',
        'products_gifts.created_at',        
        'products.p_name',
        'products.p_description',
        'products.p_rincian',
        'products.p_image',)               
    .join('products', 'products.id', 'products_gifts.product_id')  
    .where(where)  
    .orderBy('products_gifts.id', 'desc').first()
}