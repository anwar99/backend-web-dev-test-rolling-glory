const db = require("../config/connection")
const moment = require("moment")
exports.checkpoint = async (data)=>{
    var userPoint = await db('user_points').select('up_point').where({user_id:data.user_id}).first();                  
    let productGiftId = []
    var totalPoint = 0
    data.map(val =>{      
        productGiftId.push(val.product_gift_id)
    })
    let productPoint = await db('products_gifts').select('p_reedem_point').whereIn("id",productGiftId)    
    data.map((val,key) =>{      
        totalPoint = totalPoint + (productPoint[key].p_reedem_point * val.jumlah_item)
    })  
    if (userPoint.up_point < totalPoint) {
        return false
    }
    return true
}

exports.transaksiReedem = async (data)=>{
    data.map(async val =>{          
        let productPoint = await db('products_gifts').select('p_reedem_point').where("id",val.product_gift_id).first()
        var totalPointItem = val.jumlah_item * productPoint.p_reedem_point 
        await db('reedem_transaksi').insert({
            product_gift_id:val.product_gift_id,
            jumlah_item:val.jumlah_item,
            user_id:data.user_id,
            bayar_point:totalPointItem,
            created_at:moment(new Date()).format("YYYY-MM-DD")
        })        
    })
    var userPoint = await db('user_points').select('up_point').where({user_id:data.user_id}).first();                  
    let productGiftId = []
    let productId = []
    var totalPoint = 0
    data.map(val =>{          
        productGiftId.push(val.product_gift_id)
    })
    let productPoint = await db('products_gifts').select('p_reedem_point','product_id').whereIn("id",productGiftId)    
    
    data.map((val,key) =>{      
        totalPoint = totalPoint + (productPoint[key].p_reedem_point * val.jumlah_item)
        productId.push(productPoint[key].product_id)
    }) 
    var sisaPoint = userPoint.up_point - totalPoint
    await db('user_points').select('up_point').where({user_id:data.user_id}).update({
        up_point:sisaPoint
    });
    
    let stockProduct = await db('stock_product').select('sp_stock').whereIn("product_id",productId)    
    data.map(async (val,key) =>{      
        var sisaStock = stockProduct[key].sp_stock - val.jumlah_item
        await db('stock_product').where('product_id',productId[key]).update({
            "sp_stock":sisaStock
        })
    }) 
    return true
}
