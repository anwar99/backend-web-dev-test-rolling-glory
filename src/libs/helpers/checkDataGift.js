const db = require("../config/connection")

exports.checkDataGift = async (data)=>{
    return await db('products_gifts').where(data).first()
}