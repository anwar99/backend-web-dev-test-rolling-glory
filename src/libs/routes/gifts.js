var router = require('express').Router()
const {getAll, getById, createGift, updateGift, updateitemGift, deleteGift} = require('../../controllers/GiftsController')
const { ratingGift } = require('../../controllers/RatingsController')
const { reedemGift } = require('../../controllers/ReedemController')
const { authJWT } = require('../middlewares/auth')
const giftValidation = require('../validations/giftValidation')
const productValidation = require('../validations/productValidation')
const reedemValidation = require('../validations/reedemValidation')

router.get('/',authJWT,getAll)
router.post('/',authJWT,giftValidation,createGift)
router.get('/:id',authJWT,getById)
router.put('/:id',authJWT,giftValidation,updateGift)
router.patch('/:id',authJWT,productValidation,updateitemGift)
router.delete('/:id',authJWT,deleteGift)
router.post("/:id/reedem",authJWT,reedemGift)
router.post("/:id/rating",authJWT,ratingGift)

module.exports = router