var router = require('express').Router()
const {login} = require('../../controllers/LoginController')
const loginValidation = require('../validations/loginValidation')
router.post('/',loginValidation,login)

module.exports = router