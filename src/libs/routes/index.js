var routes = require('express').Router()
const db = require('../config/connection')
const user = require('./users')
const gifts = require('./gifts')

routes.use("/login",user)
routes.use("/gifts",gifts)

module.exports = routes