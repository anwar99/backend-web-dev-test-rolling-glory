const jwt = require('jsonwebtoken');
const key = require("../config/authKey")
exports.authJWT = (request,response,next)=>{
    const authHeader = request.headers.authorization;

    if (authHeader) {
        const token = authHeader.split('Bearer ')[1];
        jwt.verify(token, key.public, {           
            expiresIn:  "12h",
            algorithm:  ["RS256"]
           } ,(err, user) => {
            if (err) {
                return response.status(403).json({
                    "status": "403",
                    "error": "Token Expired"
                });
            }

            request.user = user;
            next();
        });
    } else {
        return response.status(401).json({
            "status": "401",
            "error": "Not Authorized"
        });
    }
}