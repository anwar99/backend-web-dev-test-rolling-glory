const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const helmet = require('helmet')
const app = express()
const routes = require('./libs/routes');
require('dotenv').config()
app.use(helmet())
const corsOptions = {
    origin: 'http://localhost:5000/',
    credential:true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions))
app.use(morgan("dev"))
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.use(express.static('public'))

app.use("/",routes)

module.exports = app