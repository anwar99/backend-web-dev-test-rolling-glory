const db = require("../libs/config/connection")
const moment = require("moment")
const { reviewsMapper } = require("../libs/mappers/reviewsMapper")
exports.ratingGiftService = async(data)=>{    
    var roundRating = Math.ceil(data.rating*2)/2
    if (roundRating>5) {
        return {status:500,message:"Rating Tidak Boleh Lebih dari 5"}
    }
    await db('reviews').insert({
        product_id:data.product_id,
        user_id:data.user_id,
        ratings:roundRating,
        comments:data.comments,
        created_at:moment(new Date()).format("YYYY-MM-DD")
    })

    const dataReviews = await db('reviews')        
        .join('products','products.id','reviews.product_id')
        .join('users','users.id','reviews.user_id')
        .orderBy('reviews.id','desc')
        .where({user_id:data.user_id}).first()
    return reviewsMapper(200,"Reviews Successfully Created",dataReviews)
}