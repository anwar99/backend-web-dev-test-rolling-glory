const db = require("../libs/config/connection");
const Gifts = require("../libs/dto/Gifts");
const { checkDataGift } = require("../libs/helpers/checkDataGift");
const { getAllMapper, getByIdMapper } = require("../libs/mappers/giftsMapper");
var moment = require('moment');
const { giftById } = require("../libs/helpers/giftById");

exports.getAllService = async (data)=>{           

    const datagetAll = await db('products_gifts')               
        .join('products', 'products.id', 'products_gifts.product_id')
        .join('stock_product', 'stock_product.product_id', 'products.id')                        
        .paginate({ perPage: data.query.per_page, currentPage:  data.query.current_page });    
        datagetAll.user_id = data.user_id         
    if (datagetAll.data.length>0) {
        return getAllMapper(200,"Data Gifts Found",datagetAll)        
    }else{
        return getAllMapper(404,"Data Gifts Not Found",datagetAll)
    }
}

exports.getByIdService = async (data) =>{    
    const datagetById = await db('products_gifts')               
        .join('products', 'products.id', 'products_gifts.product_id')
        .join('stock_product', 'stock_product.product_id', 'products.id')   
        .where({
            "products_gifts.id":data.id
        }).first()              
    if (datagetById) {
        return getByIdMapper(200,"Data Gifts Found",datagetById)        
    }else{
        return getByIdMapper(404,"Data Gifts Not Found",datagetById)
    }
}

exports.createGiftService = async (data)=>{
    delete data['query']
    delete data['id']
    delete data['p_name']
    delete data['p_description']
    delete data['p_rincian']
    var check = await checkDataGift(data)
    data.created_at = moment(new Date()).format("YYYY-MM-DD");
    if (check) {
        console.log(check)
        return getByIdMapper(500,"Products Already",null)
    }
    await db('products_gifts').insert(data)    
    var newGift = await giftById(data)
    return getByIdMapper(200,"Gift Successfully Created",newGift)
}

exports.updateGiftService = async (data)=>{
    delete data['query']
    delete data['p_name']
    delete data['p_description']
    delete data['p_rincian']
    data.updated_at = moment(new Date()).format("YYYY-MM-DD");
    var test = await db('products_gifts')
        .where({
            "products_gifts.id":data.id
        })
        .update(data)
    var updateGift = await giftById(data)
    console.log(updateGift)
    return getByIdMapper(200,"Gift Successfully Updated",updateGift)
}

exports.updateItemGiftService = async (data)=>{
    delete data['query']
    data.updated_at = moment(new Date()).format("YYYY-MM-DD");
    var updateGift = await giftById(data)
    delete data['id']
    delete data['product_id']
    delete data['p_reedem_point']
    var test = await db('products').where('id',updateGift.product_id).update(data)  
    var newupdateProduct = await giftById(updateGift)   
    return getByIdMapper(200,"Gift Successfully Updated Item",newupdateProduct)
}

exports.deleteGiftService = async (data)=>{
    delete data['query']
    delete data['p_name']
    delete data['p_description']
    delete data['p_rincian']
    delete data['product_id']
    delete data['query']
    var deleteData = await db('products_gifts').where('id',data.id).delete()  
    return getByIdMapper(200,"Gift Successfully Deleted",null)
}

