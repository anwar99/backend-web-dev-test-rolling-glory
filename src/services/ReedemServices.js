const {checkpoint, changePointUser, transaksiReedem } = require("../libs/helpers/reedemHelper")

exports.reedemGiftService = async (data)=>{
         
    const point = await checkpoint(data)
    if (!point) { 
        return {status:500,message:"Point Kurang"}
    }
    const transaksi = await transaksiReedem(data)
    if (transaksi) {
        return {status:200,message:"Reedem Successfully"}
    }
}