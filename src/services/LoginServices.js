const User = require("../libs/dto/User");
const db = require("../libs/config/connection")
const bcrypt = require('bcrypt');
const { userMapper } = require("../libs/mappers/userMapper");
const key = require("../libs/config/authKey")
const jwt = require('jsonwebtoken')

exports.login = async (data)=>{    
    const user = await db('users').where({
        email:data.email
    }).first()
    if (!user) {
        return userMapper(404,"User Not Found",user)
    }
    if (!bcrypt.compareSync(data.password,user.password)) {
        return userMapper(500,"Password Doesn't Match",null)
    }
    const token = jwt.sign({id:user.id,email:user.email},key.private,
        {
            expiresIn:  "12h",
            algorithm:  "RS256"
        }
   )  
   user.token = token
    return userMapper(200,"User Found",user)
}