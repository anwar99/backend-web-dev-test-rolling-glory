const Gifts = require("../libs/dto/Gifts")
const { getAllService, getByIdService, createGiftService, updateGiftService, updateItemGiftService, deleteGiftService } = require("../services/GiftsServices")

class GiftsRepository{

    constructor(data){
        this.data = new Gifts(data)
    }

    async getAllRepo(){
        return await getAllService(this.data)
    }

    async getByIdRepo(){
        return await getByIdService(this.data)
    }

    async createGiftRepo(){
        return await createGiftService(this.data)
    }

    async updateGiftRepo(){
        return await updateGiftService(this.data)
    }
    
    async updateItemGiftRepo(){
        return await updateItemGiftService(this.data)
    }

    async deleteGiftRepo(){
        return await deleteGiftService(this.data)
    }
}
module.exports = GiftsRepository