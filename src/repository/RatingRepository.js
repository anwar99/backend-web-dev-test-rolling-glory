const { ratingGiftService } = require("../services/RatingServices")

class RatingRepository{
    constructor(data){
        this.data = data.body,
        this.data.user_id = data.params.id
    }

    async ratingGiftRepo(){
        return await ratingGiftService(this.data)
    }
}

module.exports = RatingRepository