const User = require("../libs/dto/User")
const {login} = require("../services/LoginServices")

class LoginRepository{

    constructor(data){
        this.data = new User(data)
    }

    async loginRepository(){
        return await login(this.data)
    }
}

module.exports = LoginRepository