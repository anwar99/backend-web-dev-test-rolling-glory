const Reedem = require("../libs/dto/Reedem")
const { reedemGiftService } = require("../services/ReedemServices")

class ReedemRepository{
    constructor(data){
        this.data = data.body,
        this.data.user_id = data.params.id
    }

    async reedemGiftRepo(){
        return await reedemGiftService(this.data)
    }
}

module.exports = ReedemRepository