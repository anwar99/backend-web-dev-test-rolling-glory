'use strict'
const db = require("../libs/config/connection")
const GiftsRepository = require("../repository/GiftsRepository")
exports.getAll = async (request,response)=>{
    try {       
        let resGifts = new GiftsRepository(request)                        
        const gifts = await resGifts.getAllRepo()        
        return response.status(gifts.status).json(gifts)
    } catch (error) {
        return response.status(500).json(error)
    }
}

exports.getById = async (request,response)=>{
    try {
        let resGifts = new GiftsRepository(request)
        const gifts = await resGifts.getByIdRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}

exports.createGift = async (request,response)=>{
    try {        
        let resGifts = new GiftsRepository(request)
        const gifts = await resGifts.createGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}

exports.updateGift = async (request,response)=>{
    try {        
        let resGifts = new GiftsRepository(request)
        const gifts = await resGifts.updateGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}

exports.updateitemGift = async (request,response)=>{
    try {        
        let resGifts = new GiftsRepository(request)
        const gifts = await resGifts.updateItemGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}

exports.deleteGift = async (request,response)=>{
    try {        
        let resGifts = new GiftsRepository(request)
        const gifts = await resGifts.deleteGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}


