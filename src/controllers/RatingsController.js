const RatingRepository = require("../repository/RatingRepository")

exports.ratingGift = async (request,response)=>{
    try {
        let resGifts = new RatingRepository(request)
        const gifts = await resGifts.ratingGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}