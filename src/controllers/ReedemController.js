const ReedemRepository = require("../repository/ReedemRepository")

exports.reedemGift = async (request,response)=>{
    try {        
        let resGifts = new ReedemRepository(request)
        const gifts = await resGifts.reedemGiftRepo()        
        return response.status(gifts.status).json(gifts) 
    } catch (error) {
        return response.send(error)
    }
}