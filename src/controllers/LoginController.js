'use strict'

const LoginRepository = require("../repository/LoginRepository")

exports.login = async (request,response)=>{    
    try {       
        let resLogin = new LoginRepository(request.body)                        
        const log = await resLogin.loginRepository()
        return response.send(log)
    } catch (error) {
        return response.send(error)
    }
}